import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';

import { InfoModule } from '../info.module';

describe('InfoController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    process.env.SWAGGER_API_DOC_ENABLE = 'true';
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [InfoModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/info').expect(200).expect({
      status: 'OK',
    });
  });
});
