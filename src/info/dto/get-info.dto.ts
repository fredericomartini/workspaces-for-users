import { ApiProperty } from '@nestjs/swagger';
import { Info } from '../interfaces';

export class GetInfoDto implements Info {
  @ApiProperty({
    name: 'status',
    type: String,
    example: 'OK',
  })
  status: string;
}
