import { OK } from '@common/constants';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { InfoService } from './info.service';

describe('InfoService', () => {
  let service: InfoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InfoService],
    }).compile();

    service = module.get<InfoService>(InfoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it(`FEATURE: info()
      GIVEN: call info()
        THEN: should return valid json response
        AND: should have key status with value: ${HttpStatus.OK}`, () => {
    expect(service.info()).toEqual({
      status: OK,
    });
  });
});
