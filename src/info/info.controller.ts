import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';

import { GetInfoDto } from './dto';
import { InfoService } from './info.service';
import { Info } from './interfaces';

@Controller('info')
export class InfoController {
  constructor(private readonly infoService: InfoService) {}

  @Get('/')
  @ApiOperation({
    operationId: 'get_info_about_application',
    description: 'Endpoint get info about application status',
  })
  @ApiOkResponse({
    type: GetInfoDto,
  })
  info(): Info {
    return this.infoService.info();
  }
}
