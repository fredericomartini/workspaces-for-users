import { Injectable } from '@nestjs/common';

import { OK } from '@common/constants';
import { Info } from './interfaces';

@Injectable()
export class InfoService {
  info(): Info {
    return {
      status: OK,
    };
  }
}
