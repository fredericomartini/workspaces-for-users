import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Post,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

import { IsUUIDParam } from '@common/decorators';
import { BadRequestErrorDto, NotFoundErrorDto } from '@common/dto/errors';
import { ListWorkspacesByUserDto } from '@src/workspace/dto';
import { UserDto } from './dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserService } from './user.service';

@Controller('users')
@ApiTags('Users')
@UseInterceptors(ClassSerializerInterceptor)
@UsePipes(
  new ValidationPipe({
    whitelist: true,
  }),
)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiCreatedResponse({ type: UserDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({ description: 'Creates a new user' })
  @Post()
  create(@Body() createUserDto: CreateUserDto): Promise<UserDto> {
    return this.userService.create(createUserDto);
  }

  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({ description: 'Details of the User' })
  @Get(':id')
  getById(@IsUUIDParam('id') id: string): Promise<UserDto> {
    return this.userService.getById(id);
  }

  @ApiOkResponse({ type: ListWorkspacesByUserDto })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({ description: 'Workspaces that User is associated' })
  @Get(':id/workspaces')
  getByIdWithWorkspaces(
    @IsUUIDParam('id') id: string,
  ): Promise<ListWorkspacesByUserDto> {
    return this.userService.getById(id, true);
  }
}
