import { CommonEntity } from '@common/common.entity';
import { Workspace } from '@src/workspace/entities';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';

import { UserInterface } from '../interfaces';

@Entity('user')
export class User extends CommonEntity implements UserInterface {
  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  imageUrl: string;

  @ManyToMany(() => Workspace)
  @JoinTable({ name: 'workspace_users_user' })
  workspaces: Workspace[];
}
