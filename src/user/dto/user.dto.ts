import { Optional } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString, IsUrl, MinLength } from 'class-validator';

import { CommonEntityDto } from '@common/dto';
import { WorkspaceDto } from '@src/workspace/dto';
import { UserInterface } from '../interfaces';

@Exclude()
export class UserDto extends CommonEntityDto implements UserInterface {
  @ApiProperty({
    minLength: 5,
    required: true,
    example: 'Jones',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(5)
  @Expose()
  firstName: string;

  @ApiProperty({
    minLength: 5,
    required: true,
    example: 'Dyson',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(5)
  @Expose()
  lastName: string;

  @ApiProperty({
    required: true,
    example: 'https://my-website.com/picture.jpg',
  })
  @IsNotEmpty()
  @IsUrl()
  @Expose()
  imageUrl: string;

  @ApiProperty({
    type: [WorkspaceDto],
  })
  @Expose()
  @Optional()
  workspaces?: WorkspaceDto[];
}
