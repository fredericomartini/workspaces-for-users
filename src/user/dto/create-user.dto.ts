import { PickType } from '@nestjs/swagger';

import { UserCreatableInterface } from '../interfaces';
import { UserDto } from './user.dto';

export class CreateUserDto
  // PickType will pick properties as mandatory
  extends PickType(UserDto, ['firstName', 'lastName', 'imageUrl'])
  implements UserCreatableInterface {}
