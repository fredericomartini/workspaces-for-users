import { CommonEntityInterface } from '@common/interfaces';

export interface UserInterface extends CommonEntityInterface {
  firstName: string;
  lastName: string;
  imageUrl: string;
}
