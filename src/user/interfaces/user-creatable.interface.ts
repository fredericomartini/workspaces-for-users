import { UserInterface } from './user.interface';

export type UserCreatableInterface = Pick<
  UserInterface,
  'firstName' | 'lastName' | 'imageUrl'
>;
