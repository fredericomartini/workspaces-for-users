import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as request from 'supertest';
import { Repository } from 'typeorm';

import { AppModule } from '@src/app.module';
import { CreateUserDto, UserDto } from './dto';
import { User } from './entities';

describe('FEATURE: create an User (e2e)', () => {
  const PATH = '/users';
  let app: INestApplication;
  const createUserValidDto: CreateUserDto = {
    firstName: 'Jones',
    lastName: 'Dyson',
    imageUrl: 'http://some-endpoint.com.br/my-picture.jpeg',
  };

  let userRepo: Repository<User>;
  let users: User[];

  const clearUserTable = async () => {
    await userRepo.delete({});
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    userRepo = moduleFixture.get<Repository<User>>(getRepositoryToken(User));
    users = await userRepo.find();

    await clearUserTable();
  });

  afterAll(async () => {
    await clearUserTable();
    await userRepo.save(users);
    await app.close();
  });

  describe('SCENARIO: Route exists', () => {
    it(`GIVEN: Route exists
              AND: User makes a POST request to ${PATH}
                  THEN: should have proper statusCode`, async () => {
      const { statusCode } = await request(app.getHttpServer()).post(PATH);

      const allowedStatusCodes = [201, 400];
      expect(allowedStatusCodes.includes(statusCode)).toBeTruthy();
    });
  });

  describe('SCENARIO: Invalid data', () => {
    it(`GIVEN: firstName is short then 5 chars
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send({
          ...createUserValidDto,
          firstName: 'abcd',
        } as CreateUserDto);

      expect(statusCode).toBe(400);
      expect(body).toEqual({
        error: 'Bad Request',
        message: ['firstName must be longer than or equal to 5 characters'],
        statusCode: 400,
      });
    });

    it(`GIVEN: lastName is short then 5 chars
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send({
          ...createUserValidDto,
          lastName: 'abcd',
        } as CreateUserDto);

      expect(statusCode).toBe(400);
      expect(body).toEqual({
        error: 'Bad Request',
        message: ['lastName must be longer than or equal to 5 characters'],
        statusCode: 400,
      });
    });

    it(`GIVEN: imageUrl is not valid url
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send({
          ...createUserValidDto,
          imageUrl: 'htt:/invalid-url',
        } as CreateUserDto);

      expect(statusCode).toBe(400);
      expect(body).toEqual({
        error: 'Bad Request',
        message: ['imageUrl must be an URL address'],
        statusCode: 400,
      });
    });
  });

  describe('SCENARIO: Valid data', () => {
    it(`GIVEN: payload is valid
          THEN: should NOT throw Bad Request
          AND: should return statusCode = 201
          AND: should return valid createdUser data`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send(createUserValidDto);

      expect(statusCode).toBe(201);
      expect(body).toEqual({
        id: expect.any(String),
        firstName: createUserValidDto.firstName,
        lastName: createUserValidDto.lastName,
        imageUrl: createUserValidDto.imageUrl,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      } as UserDto);
    });
  });
});
