import { NotFoundError } from '@common/errors';

export class UserNotFoundError extends NotFoundError {
  constructor(extra: Record<string, any> = undefined) {
    super('User Not Found', extra);
  }
}
