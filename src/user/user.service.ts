import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { Repository } from 'typeorm';

import { UserDto } from './dto';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities';
import { UserNotFoundError } from './errors';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly repo: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserDto> {
    const createdUser = this.repo.save(this.repo.create(createUserDto));

    return plainToInstance(UserDto, createdUser);
  }

  async getById(id: string, includeWorkspaces = false): Promise<UserDto> {
    const user = await this.repo.findOne({
      where: {
        id,
      },
      relations: includeWorkspaces ? ['workspaces'] : undefined,
    });

    if (!user) {
      throw new UserNotFoundError({ id });
    }

    return plainToInstance(UserDto, user);
  }
}
