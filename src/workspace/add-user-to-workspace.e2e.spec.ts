import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as request from 'supertest';
import { Repository } from 'typeorm';

import { AppModule } from '@src/app.module';
import { CreateUserDto, UserDto } from '@src/user/dto';
import { CreateWorkspaceDto, WorkspaceDto } from './dto';
import { Workspace } from './entities';

describe('FEATURE: Add user to Workspace (e2e)', () => {
  const PATH = '/workspaces';
  let app: INestApplication;
  const createWorkspaceValidDto: CreateWorkspaceDto = {
    name: 'Workspace PhononX',
    description: 'Workspace for workspaces collaboration',
    imageUrl: 'http://some-endpoint.com.br/workspace-picture.jpeg',
  };

  const createUserValidDto: CreateUserDto = {
    firstName: 'Jones',
    lastName: 'Dyson',
    imageUrl: 'http://some-endpoint.com.br/my-picture.jpeg',
  };

  let user: UserDto;
  let workspace: WorkspaceDto;

  let repo: Repository<Workspace>;
  let workspaces: Workspace[];

  const clearWorkspaceTable = async () => {
    await repo.delete({});
  };

  const createEntity = async (path: string, data = {}) => {
    const { body } = await request(app.getHttpServer())
      .post(`/${path}`)
      .send(data);

    return body;
  };

  const createDefaultUserAndWorkspace = async () => {
    user = await createEntity('users', createUserValidDto);
    workspace = await createEntity('workspaces', createWorkspaceValidDto);
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    repo = moduleFixture.get<Repository<Workspace>>(
      getRepositoryToken(Workspace),
    );
    workspaces = await repo.find();

    await clearWorkspaceTable();

    await createDefaultUserAndWorkspace();
  });

  afterAll(async () => {
    await clearWorkspaceTable();
    await repo.save(workspaces);
    await app.close();
  });

  describe('SCENARIO: Route exists', () => {
    it(`GIVEN: Route exists
              AND: User makes a PUT request to ${PATH}
                  THEN: should have proper statusCode`, async () => {
      const { statusCode } = await request(app.getHttpServer()).put(
        `${PATH}/1234/1234`,
      );

      const allowedStatusCodes = [HttpStatus.OK, HttpStatus.BAD_REQUEST];
      expect(allowedStatusCodes.includes(statusCode)).toBeTruthy();
    });
  });

  describe('SCENARIO: Invalid data', () => {
    console.log(workspace, user);
    const validUUID = '1b90ba3b-4c64-4573-9401-75375410d9d1';

    it(`GIVEN: workspaceId is NOT valid UUID
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .put(`${PATH}/invalid-workspaceId/${validUUID}`)
        .send();

      expect(statusCode).toBe(HttpStatus.BAD_REQUEST);
      expect(body).toEqual({
        message: 'This is not a valid UUID',
        statusCode: HttpStatus.BAD_REQUEST,
      });
    });

    it(`GIVEN: userId is NOT valid UUID
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .put(`${PATH}/${validUUID}/invalid-user-id`)
        .send();

      expect(statusCode).toBe(HttpStatus.BAD_REQUEST);
      expect(body).toEqual({
        message: 'This is not a valid UUID',
        statusCode: HttpStatus.BAD_REQUEST,
      });
    });

    it(`GIVEN: workspaceId doesn't exist
          THEN: Should throw WorkspaceNotFoundError`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .put(`${PATH}/${validUUID}/${user.id}`)
        .send();

      expect(statusCode).toBe(HttpStatus.NOT_FOUND);
      expect(body).toEqual({
        error: 'Not Found',
        message: 'Workspace Not Found',
        statusCode: HttpStatus.NOT_FOUND,
        extra: {
          workspaceId: validUUID,
        },
      });
    });

    it(`GIVEN: userId doesn't exist
          THEN: Should throw UserNotFoundError`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .put(`${PATH}/${workspace.id}/${validUUID}`)
        .send();

      expect(statusCode).toBe(HttpStatus.NOT_FOUND);
      expect(body).toEqual({
        error: 'Not Found',
        message: 'User Not Found',
        statusCode: HttpStatus.NOT_FOUND,
        extra: {
          userId: validUUID,
        },
      });
    });
  });

  describe('SCENARIO: Valid data', () => {
    it(`GIVEN: payload is valid
        AND: workspace doesn't have user yet
          THEN: should NOT throw any error
          AND: should return statusCode = ${HttpStatus.OK}
          AND: should return valid associated Workspace with User data`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .put(`${PATH}/${workspace.id}/${user.id}`)
        .send(createWorkspaceValidDto);

      expect(statusCode).toBe(HttpStatus.OK);
      expect(body).toEqual({
        ...workspace,
        user,
      } as WorkspaceDto);
    });

    it(`GIVEN: payload is valid
        AND: workspace has one associated User already
          THEN: should NOT throw any error
          AND: should return statusCode = ${HttpStatus.OK}
          AND: should return valid associated Workspace with User data`, async () => {
      const secondUserValidDto: CreateUserDto = {
        ...createUserValidDto,
        firstName: 'Second Created User',
        lastName: 'Last Name',
      };
      const secondUser = await createEntity('users', secondUserValidDto);
      const { statusCode, body } = await request(app.getHttpServer())
        .put(`${PATH}/${workspace.id}/${secondUser.id}`)
        .send(createWorkspaceValidDto);

      expect(statusCode).toBe(HttpStatus.OK);
      expect(body).toEqual({
        ...workspace,
        user: secondUser,
      } as WorkspaceDto);
    });
  });
});
