import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';

import { CommonEntity } from '@common/common.entity';
import { User } from '@src/user/entities';
import { WorkspaceInterface } from '../interfaces';

@Entity('workspace')
export class Workspace extends CommonEntity implements WorkspaceInterface {
  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  imageUrl: string;

  @ManyToMany(() => User)
  @JoinTable({ name: 'workspace_users_user' })
  users: User[];

  addUser(user: User) {
    if (!this.users) {
      this.users = new Array<User>();
    }
    this.users.push(user);
  }

  removeUser(user: User) {
    this.users = this.users.filter(({ id }) => user.id !== id);
  }
}
