import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as request from 'supertest';
import { Repository } from 'typeorm';

import { AppModule } from '@src/app.module';
import { CreateWorkspaceDto, WorkspaceDto } from './dto';
import { Workspace } from './entities';

describe('FEATURE: create an Workspace (e2e)', () => {
  const PATH = '/workspaces';
  let app: INestApplication;
  const createWorkspaceValidDto: CreateWorkspaceDto = {
    name: 'Workspace PhononX',
    description: 'Workspace for workspaces collaboration',
    imageUrl: 'http://some-endpoint.com.br/workspace-picture.jpeg',
  };

  let repo: Repository<Workspace>;
  let workspaces: Workspace[];

  const clearWorkspaceTable = async () => {
    await repo.delete({});
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    repo = moduleFixture.get<Repository<Workspace>>(
      getRepositoryToken(Workspace),
    );
    workspaces = await repo.find();

    await clearWorkspaceTable();
  });

  afterAll(async () => {
    await clearWorkspaceTable();
    await repo.save(workspaces);
    await app.close();
  });

  describe('SCENARIO: Route exists', () => {
    it(`GIVEN: Route exists
              AND: User makes a POST request to ${PATH}
                  THEN: should have proper statusCode`, async () => {
      const { statusCode } = await request(app.getHttpServer()).post(PATH);

      const allowedStatusCodes = [201, 400];
      expect(allowedStatusCodes.includes(statusCode)).toBeTruthy();
    });
  });

  describe('SCENARIO: Invalid data', () => {
    it(`GIVEN: name is short then 5 chars
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send({
          ...createWorkspaceValidDto,
          name: 'abcd',
        } as CreateWorkspaceDto);

      expect(statusCode).toBe(400);
      expect(body).toEqual({
        error: 'Bad Request',
        message: ['name must be longer than or equal to 5 characters'],
        statusCode: 400,
      });
    });

    it(`GIVEN: description is short then 5 chars
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send({
          ...createWorkspaceValidDto,
          description: 'abcd',
        } as CreateWorkspaceDto);

      expect(statusCode).toBe(400);
      expect(body).toEqual({
        error: 'Bad Request',
        message: ['description must be longer than or equal to 5 characters'],
        statusCode: 400,
      });
    });

    it(`GIVEN: imageUrl is not valid url
          THEN: Should throw Bad Request`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send({
          ...createWorkspaceValidDto,
          imageUrl: 'htt:/invalid-url',
        } as CreateWorkspaceDto);

      expect(statusCode).toBe(400);
      expect(body).toEqual({
        error: 'Bad Request',
        message: ['imageUrl must be an URL address'],
        statusCode: 400,
      });
    });
  });

  describe('SCENARIO: Valid data', () => {
    it(`GIVEN: payload is valid
          THEN: should NOT throw Bad Request
          AND: should return statusCode = 201
          AND: should return valid createdUser data`, async () => {
      const { statusCode, body } = await request(app.getHttpServer())
        .post(PATH)
        .send(createWorkspaceValidDto);

      expect(statusCode).toBe(201);
      expect(body).toEqual({
        id: expect.any(String),
        name: createWorkspaceValidDto.name,
        description: createWorkspaceValidDto.description,
        imageUrl: createWorkspaceValidDto.imageUrl,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      } as WorkspaceDto);
    });
  });
});
