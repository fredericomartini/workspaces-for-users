import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  ParseArrayPipe,
  Post,
  Put,
  Query,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';

import { IsUUIDParam } from '@common/decorators';
import { BadRequestErrorDto, NotFoundErrorDto } from '@common/dto/errors';
import {
  ListWorkspacesByUserDto,
  ListWorkspacesByUserQueryDto,
  ListWorkspacesSharedByTwoUsersDto,
  ListWorkspacesSharedByTwoUsersQueryDto,
  WorkspaceDto,
  WorkspaceUserDto,
  WorkspaceUsersDto,
} from './dto';
import { CreateWorkspaceDto } from './dto/create-workspace.dto';
import { WorkspaceService } from './workspace.service';

@Controller('workspaces')
@ApiTags('Workspaces')
@UseInterceptors(ClassSerializerInterceptor)
@UsePipes(
  new ValidationPipe({
    whitelist: true,
  }),
)
export class WorkspaceController {
  constructor(private readonly service: WorkspaceService) {}

  @ApiCreatedResponse({ type: WorkspaceDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({ description: 'Creates a new workspace' })
  @Post()
  create(@Body() createUserDto: CreateWorkspaceDto): Promise<WorkspaceDto> {
    return this.service.create(createUserDto);
  }

  @ApiOkResponse({ type: [ListWorkspacesSharedByTwoUsersDto] })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiQuery({
    name: 'userIds',
    type: ListWorkspacesSharedByTwoUsersQueryDto,
    required: true,
  })
  @ApiOperation({
    description:
      'Get a list of workspaces shared between two users. Two userIds would be passed into this endpoint, and the result would be a list of workspaces that both users are a member of',
  })
  @Get('shared')
  getSharedWorkspaces(
    @Query(
      'userIds',
      new ParseArrayPipe({
        items: String,
        separator: ',',
      }),
      new ValidationPipe({
        transform: true,
      }),
    )
    userIds: string[],
  ): Promise<ListWorkspacesSharedByTwoUsersDto[]> {
    return this.service.getSharedWorkspaces(userIds as any);
  }

  @ApiOkResponse({ type: WorkspaceUserDto })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({
    description: 'Adds a user (:userId) to a workspace (:workspaceId)',
  })
  @Put(':id/:userId')
  addUserToWorkspace(
    @IsUUIDParam('id') workspaceId: string,
    @IsUUIDParam('userId') userId: string,
  ): Promise<WorkspaceUserDto> {
    return this.service.addUserToWorkspace(userId, workspaceId);
  }

  @ApiOkResponse({ type: ListWorkspacesByUserDto })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiQuery({
    name: 'userId',
    type: String,
    required: true,
  })
  @ApiOperation({
    description:
      'Get list of workspaces a particular user is a member of (need to pass a userId into the request)',
  })
  @Get()
  getAllWorkspacesByUserId(
    @Query(
      new ValidationPipe({
        transform: true,
      }),
    )
    { userId }: ListWorkspacesByUserQueryDto,
  ): Promise<ListWorkspacesByUserDto> {
    return this.service.getAllWorkspacesByUserId(userId);
  }

  @ApiOkResponse({ type: WorkspaceUsersDto })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({
    description:
      'Get details of indicated workspace (including all users that are part of the workspace)',
  })
  @Get(':id')
  getByIdWithUsers(@IsUUIDParam('id') id: string): Promise<WorkspaceUsersDto> {
    return this.service.getById(id, true);
  }

  @ApiNoContentResponse({ description: 'No Content' })
  @ApiNotFoundResponse({ type: NotFoundErrorDto })
  @ApiBadRequestResponse({ type: BadRequestErrorDto })
  @ApiOperation({
    description: 'Removes a user (:userId) from a workspace (:workspaceId)',
  })
  @Delete(':id/:userId')
  removeUserFromWorkspace(
    @IsUUIDParam('id') workspaceId: string,
    @IsUUIDParam('userId') userId: string,
  ): Promise<void> {
    return this.service.removeUserFromWorkspace(userId, workspaceId);
  }
}
