import { CommonEntityInterface } from '@common/interfaces';

export interface WorkspaceInterface extends CommonEntityInterface {
  name: string;
  description: string;
  imageUrl: string;
}
