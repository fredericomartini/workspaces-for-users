import { WorkspaceInterface } from './workspace.interface';

export type WorkspaceCreatableInterface = Pick<
  WorkspaceInterface,
  'name' | 'description' | 'imageUrl'
>;
