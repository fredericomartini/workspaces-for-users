import { PickType } from '@nestjs/swagger';
import { WorkspaceCreatableInterface } from '../interfaces';
import { WorkspaceDto } from './workspace.dto';

export class AddUserToWorkspaceDto
  // PickType will pick properties as mandatory
  extends PickType(WorkspaceDto, ['name', 'description', 'imageUrl'])
  implements WorkspaceCreatableInterface {}
