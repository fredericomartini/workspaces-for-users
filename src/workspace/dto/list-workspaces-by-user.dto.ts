import { UserDto } from '@src/user/dto';
import { Exclude } from 'class-transformer';

@Exclude()
export class ListWorkspacesByUserDto extends UserDto {}
