import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class ListWorkspacesSharedByTwoUsersQueryDto {
  @ApiProperty({
    example: [
      'd8539442-5fc7-4a77-8454-e79af7bd6546',
      '5e1b3394-2a2b-4e65-9028-22aab4dda339',
    ],
  })
  @IsUUID(undefined, { each: true })
  userIds: string[];
}
