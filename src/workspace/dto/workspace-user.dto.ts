import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { UserDto } from '@src/user/dto';
import { Exclude, Expose } from 'class-transformer';
import { WorkspaceInterface } from '../interfaces';
import { WorkspaceDto } from './workspace.dto';

@Exclude()
export class WorkspaceUserDto
  extends WorkspaceDto
  implements WorkspaceInterface
{
  @ApiProperty({
    type: UserDto,
  })
  @IsNotEmpty()
  @Expose()
  user: UserDto;
}
