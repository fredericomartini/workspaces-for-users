import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUrl, MinLength } from 'class-validator';

import { CommonEntityDto } from '@common/dto';
import { Exclude, Expose } from 'class-transformer';
import { WorkspaceInterface } from '../interfaces';

@Exclude()
export class WorkspaceDto
  extends CommonEntityDto
  implements WorkspaceInterface
{
  @ApiProperty({
    minLength: 5,
    required: true,
    example: 'Workspace PhononX',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(5)
  @Expose()
  name: string;

  @ApiProperty({
    minLength: 5,
    required: true,
    example: 'Workspace to collaborate with other users',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(5)
  @Expose()
  description: string;

  @ApiProperty({
    required: true,
    example: 'https://my-website.com/picture.jpg',
  })
  @IsNotEmpty()
  @IsUrl()
  @Expose()
  imageUrl: string;
}
