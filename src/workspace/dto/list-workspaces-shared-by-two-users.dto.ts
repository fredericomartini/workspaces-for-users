import { ApiProperty } from '@nestjs/swagger';
import { UserDto } from '@src/user/dto';
import { Expose } from 'class-transformer';
import { WorkspaceInterface } from '../interfaces';
import { WorkspaceDto } from './workspace.dto';

export class ListWorkspacesSharedByTwoUsersDto
  extends WorkspaceDto
  implements WorkspaceInterface
{
  @ApiProperty({
    type: [UserDto],
  })
  @Expose()
  users: UserDto[];
}
