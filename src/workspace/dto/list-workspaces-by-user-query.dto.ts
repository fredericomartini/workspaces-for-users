import { IsNotEmpty, IsUUID } from 'class-validator';

export class ListWorkspacesByUserQueryDto {
  @IsNotEmpty()
  @IsUUID()
  userId!: string;
}
