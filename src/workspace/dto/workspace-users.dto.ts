import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

import { UserDto } from '@src/user/dto';
import { WorkspaceInterface } from '../interfaces';
import { WorkspaceDto } from './workspace.dto';

@Exclude()
export class WorkspaceUsersDto
  extends WorkspaceDto
  implements WorkspaceInterface
{
  @ApiProperty({
    type: [UserDto],
  })
  @Expose()
  users?: UserDto[];
}
