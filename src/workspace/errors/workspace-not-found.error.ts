import { NotFoundError } from '@common/errors';

export class WorkspaceNotFoundError extends NotFoundError {
  constructor(extra: Record<string, any> = undefined) {
    super('Workspace Not Found', extra);
  }
}
