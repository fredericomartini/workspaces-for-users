import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@src/user/entities';
import { UserNotFoundError } from '@src/user/errors';
import { UserService } from '@src/user/user.service';
import { plainToInstance } from 'class-transformer';
import { intersection, map } from 'lodash';
import { In, Repository } from 'typeorm';

import {
  ListWorkspacesByUserDto,
  ListWorkspacesSharedByTwoUsersDto,
  WorkspaceDto,
  WorkspaceUserDto,
  WorkspaceUsersDto,
} from './dto';
import { CreateWorkspaceDto } from './dto/create-workspace.dto';
import { Workspace } from './entities';
import { WorkspaceNotFoundError } from './errors';

@Injectable()
export class WorkspaceService {
  constructor(
    @InjectRepository(Workspace)
    private readonly repo: Repository<Workspace>,
    private readonly userService: UserService,
  ) {}
  async create(createWorkspaceDto: CreateWorkspaceDto): Promise<WorkspaceDto> {
    const createdUser = this.repo.save(this.repo.create(createWorkspaceDto));

    return plainToInstance(WorkspaceDto, createdUser);
  }

  async addUserToWorkspace(
    userId: string,
    workspaceId: string,
  ): Promise<WorkspaceUserDto> {
    const workspace = await this.repo.findOne({
      where: { id: workspaceId },
      relations: ['users'],
    });

    if (!workspace) {
      throw new WorkspaceNotFoundError({ workspaceId });
    }

    workspace.addUser({ ...new User(), id: userId });

    try {
      await this.repo.save(workspace);
    } catch (error) {
      throw new UserNotFoundError({ userId });
    }

    const { users, ...workspaceWithUser } = await this.repo.findOneOrFail({
      where: { id: workspaceId, users: { id: userId } },
      relations: ['users'],
    });

    const [user] = users;

    return plainToInstance(
      WorkspaceUserDto,
      {
        ...workspaceWithUser,
        user,
      },
      {
        enableImplicitConversion: true,
      },
    );
  }

  async getAllWorkspacesByUserId(
    userId: string,
  ): Promise<ListWorkspacesByUserDto> {
    const userWithWorkspaces = await this.userService.getById(userId, true);

    return plainToInstance(ListWorkspacesByUserDto, userWithWorkspaces);
  }

  async getById(id: string, includeUsers = false): Promise<WorkspaceUsersDto> {
    const workspace = await this.repo.findOne({
      where: {
        id,
      },
      relations: includeUsers ? ['users'] : undefined,
    });

    if (!workspace) {
      throw new UserNotFoundError({ id });
    }

    return plainToInstance(WorkspaceUsersDto, workspace);
  }

  async removeUserFromWorkspace(
    userId: string,
    workspaceId: string,
  ): Promise<void> {
    const workspace = await this.repo.findOne({
      where: { id: workspaceId },
      relations: ['users'],
    });

    if (!workspace?.users) {
      throw new WorkspaceNotFoundError({ workspaceId, userId });
    }

    workspace.removeUser({ ...new User(), id: userId });

    await this.repo.save(workspace);
  }

  async getSharedWorkspaces([userIdOne, userIdTwo]: string[]): Promise<
    ListWorkspacesSharedByTwoUsersDto[]
  > {
    // Get only the equal between two users
    const workspaceIds = intersection(
      await this.getWorkspaceIdsByUser(userIdOne),
      await this.getWorkspaceIdsByUser(userIdTwo),
    );

    const sharedWorkspaces = await this.repo.find({
      where: [{ id: In(workspaceIds) }],
      relations: ['users'],
    });

    return plainToInstance(ListWorkspacesSharedByTwoUsersDto, sharedWorkspaces);
  }

  private async getWorkspaceIdsByUser(userId: string): Promise<string[]> {
    return map(
      await this.repo.find({
        where: [{ users: { id: userId } }],
        select: ['id'],
      }),
      'id',
    );
  }
}
