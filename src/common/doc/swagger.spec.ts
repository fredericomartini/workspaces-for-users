import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { swagger } from './swagger';

describe('FEATURE: swagger()', () => {
  let spyBuild: jest.SpyInstance;
  let spyCreateDocument: jest.SpyInstance;
  let spySetup: jest.SpyInstance;
  const app: any = {};

  beforeEach(async () => {
    spyBuild = jest
      .spyOn(DocumentBuilder.prototype, 'build')
      .mockImplementation(jest.fn());
    spyCreateDocument = jest
      .spyOn(SwaggerModule, 'createDocument')
      .mockImplementation(jest.fn());
    spySetup = jest.spyOn(SwaggerModule, 'setup').mockImplementation(jest.fn());
  });

  describe('SCENARIO: process.env.SWAGGER_API_DOC_ENABLE !== "true"', () => {
    beforeAll(async () => {
      process.env.SWAGGER_API_DOC_ENABLE = 'any-value';
    });

    it(`GIVEN: call swagger()
          WHEN: process.env.SWAGGER_API_DOC_ENABLE !== "true"
            THEN: should NOT call "build()" FROM DocumentBuilder
            AND: should NOT call "createDocument()" FROM SwaggerModule
            AND: should NOT call "setup()" from SwaggerModule`, () => {
      swagger(app);

      expect(spyBuild).not.toHaveBeenCalled();
      expect(spyCreateDocument).not.toHaveBeenCalled();
      expect(spySetup).not.toHaveBeenCalled();
    });
  });

  describe('SCENARIO: process.env.SWAGGER_API_DOC_ENABLE === "true"', () => {
    beforeAll(async () => {
      process.env.SWAGGER_API_DOC_ENABLE = 'true';
    });

    it(`GIVEN: call swagger()
          WHEN: process.env.SWAGGER_API_DOC_ENABLE === "true"
            THEN: should call "build()" FROM DocumentBuilder
            AND: should call "createDocument()" FROM SwaggerModule
            AND: should call "setup()" from SwaggerModule`, () => {
      const docOptions = 'doc-options';
      spyBuild.mockReturnValue(docOptions);

      swagger(app);

      expect(spyBuild).toHaveBeenCalled();
      expect(spyCreateDocument).toHaveBeenCalledWith(app, docOptions);
      expect(spySetup).toHaveBeenCalled();
    });

    it(`GIVEN: call swagger()
          WHEN: process.env.SWAGGER_API_DOC_ENABLE === "true"
            THEN: should call "build()" FROM DocumentBuilder
            AND: should call "createDocument()" FROM SwaggerModule
            AND: should call "setup()" from SwaggerModule
            AND: should use "docs" as path`, () => {
      const docOptions = 'doc-options';
      const document = 'document';

      spyBuild.mockReturnValue(docOptions);
      spyCreateDocument.mockReturnValue(document);

      swagger(app);

      expect(spyBuild).toHaveBeenCalled();
      expect(spyCreateDocument).toHaveBeenCalledWith(app, docOptions);
      expect(spySetup).toHaveBeenCalledWith('docs', app, document);
    });
  });
});
