import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';
import { serverConfig } from '@common/config';

export async function swagger(app: INestApplication) {
  const appServerConfig = serverConfig();

  if (!appServerConfig.swaggerEnabled) {
    return;
  }

  // document options
  const docOptions = new DocumentBuilder()
    .setTitle('Workspace for Users')
    .setVersion('1.0')
    .build();

  // create document
  const document = SwaggerModule.createDocument(app, docOptions);

  // set up docs route
  SwaggerModule.setup('docs', app, document);
}
