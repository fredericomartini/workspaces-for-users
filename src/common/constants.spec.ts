import { OK, SERVER_CONFIG, TYPEORM_MODULE_CONFIG } from './constants';

describe('Unity tests for constants', () => {
  it(`GIVEN: const "SERVER_CONFIG"
        THEN: should have valid value`, async () => {
    expect(SERVER_CONFIG).toBe('SERVER_CONFIG');
  });

  it(`GIVEN: const "TYPEORM_MODULE_CONFIG"
        THEN: should have valid value`, async () => {
    expect(TYPEORM_MODULE_CONFIG).toBe('TYPEORM_MODULE_CONFIG');
  });

  it(`GIVEN: const "OK"
        THEN: should have valid value`, async () => {
    expect(OK).toBe('OK');
  });
});
