export interface CommonErrorInterface {
  error: string;
  message: string | string[];
  statusCode: number;
  extra?: Record<string, any>;
}
