export interface ServerConfigInterface {
  environment: string;
  swaggerEnabled: boolean;
  port: number;
}
