import { CommonEntityInterface } from '@common/interfaces';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';

@Exclude()
export class CommonEntityDto implements CommonEntityInterface {
  @Expose()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: 'Entity unique identifier',
    example: 'f1cb28f9-9b00-4b27-a9e1-a7c5d0ea1b21',
  })
  id!: string;

  @Expose()
  @IsNotEmpty()
  @ApiProperty({
    type: 'date-time',
    required: true,
    description: 'The date and time at which the resource was created.',
    example: '2023-01-15T00:30:00Z',
  })
  createdAt!: Date;

  @Expose()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    type: 'date-time',
    description: 'The date and time at which the resource was last updated.',
    example: '2023-01-16T00:30:00Z',
  })
  updatedAt!: Date;
}
