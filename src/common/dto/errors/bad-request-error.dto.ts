import { CommonErrorInterface } from '@common/interfaces';
import { HttpStatus } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { CommonErrorDto } from './common-error.dto';

export class BadRequestErrorDto
  extends CommonErrorDto
  implements CommonErrorInterface
{
  @Expose()
  @ApiProperty({
    description: 'The generated Error',
    example: 'Bad Request',
  })
  error!: string;

  @Expose()
  @ApiProperty({
    description: 'The generated Error Message',
    example: 'name must be longer than or equal to 5 characters',
  })
  message!: string;

  @Expose()
  @ApiProperty({
    description: 'The StatusCode of the error',
    example: HttpStatus.BAD_REQUEST,
  })
  statusCode!: number;
}
