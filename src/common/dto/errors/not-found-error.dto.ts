import { CommonErrorDto } from './common-error.dto';

export class NotFoundErrorDto extends CommonErrorDto {}
