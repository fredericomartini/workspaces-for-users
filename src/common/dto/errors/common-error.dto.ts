import { HttpStatus } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

import { CommonErrorInterface } from '@common/interfaces';

@Exclude()
export class CommonErrorDto implements CommonErrorInterface {
  @Expose()
  @ApiProperty({
    description: 'The generated Error',
    example: 'Not Found',
  })
  error!: string;

  @Expose()
  @ApiProperty({
    description: 'The generated Error Message',
    example: 'User Not Found with this ID',
  })
  message!: string;

  @Expose()
  @ApiProperty({
    description: 'The StatusCode of the error',
    example: HttpStatus.NOT_FOUND,
  })
  statusCode!: number;

  @Expose()
  @ApiProperty({
    description: 'Extra information about the error',
    example: {
      userId: 'xxxxx.xxxx.xxxx.xxxx',
    },
  })
  extra?: Record<string, any>;
}
