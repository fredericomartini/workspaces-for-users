import { HttpStatus, NotFoundException } from '@nestjs/common';

export abstract class NotFoundError extends NotFoundException {
  constructor(message = 'Not Found', extra: Record<string, any> = undefined) {
    super({
      error: 'Not Found',
      message,
      statusCode: HttpStatus.NOT_FOUND,
      extra,
    });
  }
}
