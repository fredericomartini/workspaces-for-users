import { registerAs } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

import { TYPEORM_MODULE_CONFIG } from '..';

type DB_TYPE = 'postgres';
type DB_LOGGER = 'advanced-console' | 'simple-console' | 'file' | 'debug';

export const typeormConfig = registerAs(
  TYPEORM_MODULE_CONFIG,
  (): TypeOrmModuleOptions => {
    return {
      type: process.env.DB_TYPE as DB_TYPE,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      synchronize: process.env.DB_SYNCHRONIZE === 'true',
      autoLoadEntities: true,
      logging: process.env.DB_DEBUG === 'true',
      logger: (process.env.DB_LOGGER as DB_LOGGER) || 'advanced-console',
    };
  },
);
