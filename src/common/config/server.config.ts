import { registerAs } from '@nestjs/config';

import { SERVER_CONFIG } from '@common/constants';
import { ServerConfigInterface } from '@common/interfaces';

export const serverConfig = registerAs(
  SERVER_CONFIG,
  (): ServerConfigInterface => ({
    environment: process.env?.NODE_ENV ?? 'dev',
    swaggerEnabled: process.env.SWAGGER_API_DOC_ENABLE === 'true',
    port:
      'string' === typeof process.env.PORT ? parseInt(process.env.PORT) : 3000,
  }),
);
