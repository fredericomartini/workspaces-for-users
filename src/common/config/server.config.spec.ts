import { serverConfig } from './server.config';

describe(`FEATURE: serverConfig()`, () => {
  describe('GIVEN: call serverConfig', () => {
    describe('SCENARIO: testing NODE_ENV var', () => {
      it(`WHEN: process.env.NODE_ENV is NOT defined
          THEN: should have environment as "dev"`, async () => {
        delete process.env.NODE_ENV;

        const config = serverConfig();

        expect(config.environment).toBe('dev');
      });

      it(`WHEN: process.env.NODE_ENV is defined
          THEN: should have defined value`, async () => {
        process.env.NODE_ENV = 'ANY-VALUE';

        const config = serverConfig();

        expect(config.environment).toBe('ANY-VALUE');
      });
    });

    describe('SCENARIO: testing PORT var', () => {
      it(`WHEN: process.env.PORT is NOT defined
            THEN: should have PORT equal to 3000`, async () => {
        delete process.env.PORT;

        const config = serverConfig();

        expect(config.port).toBe(3000);
      });

      it(`WHEN: process.env.PORT is defined
            THEN: should have defined value`, async () => {
        process.env.PORT = '3333';

        const config = serverConfig();

        expect(config.port).toBe(3333);
      });
    });

    describe('SCENARIO: testing swaggerEnabled var', () => {
      it(`WHEN: process.env.SWAGGER_API_DOC_ENABLE is NOT defined
            THEN: should have swaggerEnabled equal to false`, async () => {
        delete process.env.SWAGGER_API_DOC_ENABLE;

        const config = serverConfig();

        expect(config.swaggerEnabled).toBeFalsy();
      });

      it(`WHEN: process.env.SWAGGER_API_DOC_ENABLE is false
            THEN: should have swaggerEnabled equal to false`, async () => {
        delete process.env.SWAGGER_API_DOC_ENABLE;

        const config = serverConfig();

        expect(config.swaggerEnabled).toBeFalsy();
      });

      it(`WHEN: process.env.SWAGGER_API_DOC_ENABLE is true
            THEN: should have swaggerEnabled equal to true`, async () => {
        process.env.SWAGGER_API_DOC_ENABLE = 'true';

        const config = serverConfig();

        expect(config.swaggerEnabled).toBeTruthy();
      });
    });
  });
});
