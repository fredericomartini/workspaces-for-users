import { NestFactory } from '@nestjs/core';

import { swagger } from '@common/doc';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Register swagger based on .env var
  await swagger(app);

  await app.listen(3000);
}
bootstrap();
