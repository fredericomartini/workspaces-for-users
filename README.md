# Workspace for Users

# Prerequisites
- [Docker](https://docs.docker.com/get-docker/)
- [Docker-compose](https://docs.docker.com/compose/install/linux/)
- [Node.JS](https://nodejs.org/en/download/) [16]

# How to run
1 - Clone repository
```bash
git clone https://github.com/fredericomartini/workspace-for-users.git
```

2 - Install dependencies
```bash
npm install
```

3 - Create a copy `.env`
```bash
cp .env.sample .env
```

4 - Start the app
```bash
# Will setup automatic docker-compose and create default database
npm run start:dev
```

5 - Confirm that is running
```bash
curl 'http://localhost:3000/info'
# Expected output: {"status":"OK"}
```



# Docs
- Swagger: [Localhost:3000 Docs](http://localhost:3000/docs)
- Postman: `./docs/postman` (just needs to import collection and environment variables)


# Done:
| Routes | Description | Status |
|---|---|---|
| POST /workspace | Creates a new workspace | ✅ |
| GET /workspaces | Get list of workspaces a particular user is a member of (need to pass a userId into the request) | ✅ |
| GET /workspace/:workspaceid | Get details of indicated workspace (including all  users that are part of the workspace) | ✅ |
| POST /user | Creates a new user | ✅ |
| PUT /workspace/:workspaceid/:userid | Adds a user (:userid) to a workspace  (:workspaceid) | ✅ |
| DELETE /workspace/:workspaceid/:userid | Removes a user (:userid) from a workspace  (:workspaceid) | ✅ |
| GET /workspaces/shared | Get a list of workspaces shared between two users. Two userids would be passed into this endpoint, and the result would be a list of workspaces that both users are a member of | ✅ |

### Screenshots:
- Swagger:
![01](./docs/swagger/swagger-routes.png)

- Create workspace:
![02](./docs/swagger/create-workspace.png)

- Create user:
![03](./docs/swagger/create-user.png)

- Details of one workspace:
![04](./docs/swagger/01.png)

- Add user to workspace:
![05](./docs/swagger/add-user-to-workspace.png)

- remove user from workspace
![06](./docs/swagger/delete.png)

- list shared workspaces between 2 users
![07](./docs/swagger/list-shared-workspaces-between-users.png)

- user details
![08](./docs/swagger/user-details.png)

- user workspaces
![09](./docs/swagger/workspaces-user-associated.png)

- workspaces by user
![10](./docs/swagger/workspaces-by-user.png))

- ER:
![ER](./docs/db-er.png)

# Questions:

- Is what you created scalable to 10 million workspaces and 10 million users? If not, what would
have to change?
    - I believe that only making load tests we can ensure that.
    - Some improvements that I would made:

      - Add pagination while loading associations, to reduce the database overhead.
      - Add cache layer to prevent hitting database to frequent.
      - Changing PostgreSQL to NoSQL database (DynamoDB) could be a good option, using right access patterns, should be very performant and highly scalable.

- Does the performance of your solution change as the number of workspaces increases? As the
number of users increases? If so, why is that and what would have to change to keep constant
performance?

    - Yes. The principal reason is not having pagination and cache layer. As commented above, only making load tests to have more guarantee. Adding pagination and cache Layer (basic and simple steps) could add good performance impact.

- What are the limitations of your solution?

    - Returning all users associated with one workspace or returning all workspaces that one user has is not good without paginating.
    - Adding pagination and cache could improve performance while database grows.

- How do you handle different types of errors?

    - Validating input payloads
    - Throwing Custom errors
    - Having clear information about what is wrong.